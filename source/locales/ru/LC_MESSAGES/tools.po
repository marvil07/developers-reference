
msgid ""
msgstr ""
"Project-Id-Version: developers-reference \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-05-05 17:55+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../tools.rst:4
msgid "Overview of Debian Maintainer Tools"
msgstr "Обзор инструментов Debian для сопровождающего"

#: ../tools.rst:6
msgid ""
"This section contains a rough overview of the tools available to "
"maintainers. The following is by no means complete or definitive, but "
"just a guide to some of the more popular tools."
msgstr ""
"Данный раздел содержит краткий обзор доступных сопровождающим "
"инструментов. Приведённая ниже информация ни в коем мере не является "
"полной или окончательной, она представляет собой лишь руководство по "
"некоторым более популярным утилитам."

#: ../tools.rst:10
msgid ""
"Debian maintainer tools are meant to aid developers and free their time "
"for critical tasks. As Larry Wall says, there's more than one way to do "
"it."
msgstr ""
"Инструменты сопровождающего Debian предназначены для облегчения работы "
"разработчиком и освобождения их времени для решения критических задач. "
"Как говорит Ларри Уолл, существует более одного способа сделать это."

#: ../tools.rst:14
msgid ""
"Some people prefer to use high-level package maintenance tools and some "
"do not. Debian is officially agnostic on this issue; any tool that gets "
"the job done is fine. Therefore, this section is not meant to stipulate "
"to anyone which tools they should use or how they should go about their "
"duties of maintainership. Nor is it meant to endorse any particular tool "
"to the exclusion of a competing tool."
msgstr ""

#: ../tools.rst:21
#, fuzzy
msgid ""
"Most of the descriptions of these packages come from the actual package "
"descriptions themselves. Further information can be found in the package "
"documentation itself. You can also see more info with the command ``apt-"
"cache show`` *package-name*."
msgstr ""
"Большинство описаний этих пакетов берётся из самих фактических описаний "
"пакетов. Дополнительная информация может быть найдена в самой "
"документации по пакету. Также вы можете получить дополнительную "
"информацию при помощи команды ``apt-cache show``\\ *имя-пакета*."

#: ../tools.rst:29
msgid "Core tools"
msgstr "Базовые инструменты"

#: ../tools.rst:31
msgid "The following tools are pretty much required for any maintainer."
msgstr "Следующие инструменты весьма нужны любому сопровождающими."

#: ../tools.rst:34
msgid "``dpkg-dev``"
msgstr ""

#: ../tools.rst:36
msgid ""
"``dpkg-dev`` contains the tools (including ``dpkg-source``) required to "
"unpack, build, and upload Debian source packages. These utilities contain"
" the fundamental, low-level functionality required to create and "
"manipulate packages; as such, they are essential for any Debian "
"maintainer."
msgstr ""
"``dpkg-dev`` содержит инструменты (включая ``dpkg-source``), необходимые "
"для распаковки, сборки и загрузки пакетов Debian с исходным кодом. Эти "
"утилиты предоставляют базовую, низкоуровневую функциональность, "
"необходимую для создания пакетов и для манипуляции ими; как таковые, эти "
"утилиты являются необходимыми любому сопровождающему Debian."

#: ../tools.rst:43
msgid "``debconf``"
msgstr ""

#: ../tools.rst:45
msgid ""
"``debconf`` provides a consistent interface to configuring packages "
"interactively. It is user interface independent, allowing end-users to "
"configure packages with a text-only interface, an HTML interface, or a "
"dialog interface. New interfaces can be added as modules."
msgstr ""
"``debconf`` предоставляет единообразный интерфейс для интерактивной "
"настройки пакетов. Его пользовательских интерфейс независим, что "
"позволяет конечным пользователям настраивать пакеты при помощи текстового"
" интерфейса, интерфейса HTML, либо диалогового интерфейса. Новые варианты"
" интерфейса могут быть добавлены в виде модулей."

#: ../tools.rst:50
msgid ""
"You can find documentation for this package in the ``debconf-doc`` "
"package."
msgstr "Документацию для этого пакета вы можете найти в пакете ``debconf-doc``."

#: ../tools.rst:53
msgid ""
"Many feel that this system should be used for all packages that require "
"interactive configuration; see :ref:`bpp-config-mgmt`. ``debconf`` is not"
" currently required by Debian Policy, but that may change in the future."
msgstr ""

#: ../tools.rst:59
msgid "``fakeroot``"
msgstr ""

#: ../tools.rst:61
msgid ""
"``fakeroot`` simulates root privileges. This enables you to build "
"packages without being root (packages usually want to install files with "
"root ownership). If you have ``fakeroot`` installed, ``dpkg-"
"buildpackage`` will use it automatically."
msgstr ""
"``fakeroot`` имитирует привилегии суперпользователя. Это позволяет вам "
"собирать пакеты без привилегий суперпользователя (обычно пакеты хотят "
"установить файлы, владельцем которых является суперпользователь). Если у "
"вас установлен ``fakeroot``, ``dpkg-buildpackage`` будет автоматически "
"использовать его."

#: ../tools.rst:69
msgid "Package lint tools"
msgstr ""
"Инструменты для проверки пакетов на предмет ошибок и соответствия "
"стандартам"

#: ../tools.rst:71
msgid ""
"According to the Free On-line Dictionary of Computing (FOLDOC), ``lint`` "
"is: \"A Unix C language processor which carries out more thorough checks "
"on the code than is usual with C compilers.\" Package lint tools help "
"package maintainers by automatically finding common problems and policy "
"violations in their packages."
msgstr ""

#: ../tools.rst:80
msgid "``lintian``"
msgstr ""

#: ../tools.rst:82
msgid ""
"``lintian`` dissects Debian packages and emits information about bugs and"
" policy violations. It contains automated checks for many aspects of "
"Debian policy as well as some checks for common errors."
msgstr ""
"``lintian`` анализирует пакеты Debian и выдаёт информацию об ошибках и "
"нарушениях Политики. Он содержит автоматические проверки множества "
"аспектов Политики Debian, а также некоторые проверки на наличие "
"распространённых ошибок."

#: ../tools.rst:86
msgid ""
"You should periodically get the newest ``lintian`` from ``unstable`` and "
"check over all your packages. Notice that the ``-i`` option provides "
"detailed explanations of what each error or warning means, what its basis"
" in Policy is, and commonly how you can fix the problem."
msgstr ""
"Периодически вам следует получить наиболее свежую версию ``lintian`` из "
"``нестабильного`` выпуска и проверять все ваши пакеты. Заметьте, что "
"опция ``-i`` предоставляет подробное объяснение того, что означает каждая"
" ошибка или предупреждение, на каком пункте Политики они основываются, а "
"иногда и то, как вы можете исправить проблему."

#: ../tools.rst:91
msgid ""
"Refer to :ref:`sanitycheck` for more information on how and when to use "
"Lintian."
msgstr ""
"Для получения дополнительной информации о том, как и когда использовать "
"Lintian, обратитесь к :ref:`sanitycheck`."

#: ../tools.rst:94
msgid ""
"You can also see a summary of all problems reported by Lintian on your "
"packages at https://lintian.debian.org/\\ . These reports contain the "
"latest ``lintian`` output for the whole development distribution "
"(``unstable``)."
msgstr ""
"Также вы можете посмотреть обзор всех проблем, о которых было сообщено "
"Lintian для ваших пакетов, по адресу https://lintian.debian.org/\\ . Эти "
"отчёты содержат наиболее свежий вывод ``lintian`` для всего "
"разрабатываемого выпуска (``нестабильного`` выпуска)."

#: ../tools.rst:100
msgid "``lintian-brush``"
msgstr ""

#: ../tools.rst:102
msgid ""
"``lintian-brush`` contains a set of scripts that can automatically fix "
"more than 80 common lintian issues in Debian packages."
msgstr ""

#: ../tools.rst:105
msgid ""
"It comes with a wrapper script that invokes the scripts, updates the "
"changelog (if desired) and commits each change to version control."
msgstr ""

#: ../tools.rst:109
msgid "``piuparts``"
msgstr ""

#: ../tools.rst:111
msgid ""
"``piuparts`` is the  ``.deb`` package installation, upgrading, and "
"removal testing tool."
msgstr ""

#: ../tools.rst:114
msgid ""
"``piuparts`` tests that ``.deb packages`` handle installation, upgrading,"
" and removal correctly. It does this by creating a minimal Debian "
"installation in a chroot, and installing, upgrading, and removing "
"packages in that environment, and comparing the state of the directory "
"tree before and after. ``piuparts`` reports any files that have been "
"added, removed, or modified during this process."
msgstr ""

#: ../tools.rst:121
msgid ""
"``piuparts`` is meant as a quality assurance tool for people who create "
"``.deb`` packages to test them before they upload them to the Debian "
"archive."
msgstr ""

#: ../tools.rst:128
msgid "``debdiff``"
msgstr ""

#: ../tools.rst:130
msgid ""
"``debdiff`` (from the ``devscripts`` package, :ref:`devscripts`) compares"
" file lists and control files of two packages. It is a simple regression "
"test, as it will help you notice if the number of binary packages has "
"changed since the last upload, or if something has changed in the control"
" file. Of course, some of the changes it reports will be all right, but "
"it can help you prevent various accidents."
msgstr ""
"``debdiff`` (из пакета ``devscripts``, :ref:`devscripts`) сравнивает "
"списки файлов и управляющие файлы двух пакетов. Это простая проверка на "
"наличие регрессий, поскольку она позволяет вам заметить, что число "
"двоичных пакетов изменилось с момента последней загрузки, либо если "
"что-то было изменено в управляющем файле. Конечно, некоторые изменения, о"
" которых сообщает эта утилита, не являются чем-то плохим, но она может "
"помочь вам предотвратить различные случайные проблемы."

#: ../tools.rst:137
msgid "You can run it over a pair of binary packages:"
msgstr "Вы можете запустить её, указав два двоичных пакета:"

#: ../tools.rst:143
msgid "Or even a pair of changes files:"
msgstr "Или даже два файла changes:"

#: ../tools.rst:149
msgid "For more information please see debdiff 1."
msgstr "Дополнительную информацию см. в debdiff 1."

#: ../tools.rst:154
msgid "``diffoscope``"
msgstr ""

#: ../tools.rst:156
msgid ""
"``diffoscope`` provides in-depth comparison of files, archives, and "
"directories."
msgstr ""

#: ../tools.rst:158
msgid ""
"``diffoscope`` will try to get to the bottom of what makes files or "
"directories different. It will recursively unpack archives of many kinds "
"and transform various binary formats into more human readable form to "
"compare them."
msgstr ""

#: ../tools.rst:162
msgid ""
"Originally developed to compare two ``.deb`` files or two ``changes`` "
"files nowadays it can compare two tarballs, ISO images, or PDF just as "
"easily and supports a huge variety of filetypes."
msgstr ""

#: ../tools.rst:166
msgid "The differences can be shown in a text or HTML report or as JSON output."
msgstr ""

#: ../tools.rst:169
msgid "``duck``"
msgstr ""

#: ../tools.rst:171
msgid ""
"``duck``, the Debian Url ChecKer, processes several fields in the "
"``debian/control``, ``debian/upstream``, ``debian/copyright``, "
"``debian/patches/*`` and ``systemd.unit`` files and checks if URLs, VCS "
"links and email address domains found therein are valid."
msgstr ""

#: ../tools.rst:177
msgid "``adequate``"
msgstr ""

#: ../tools.rst:179
msgid ""
"``adequate`` checks packages installed on the system and reports bugs and"
" policy violations."
msgstr ""

#: ../tools.rst:182
msgid "The following checks are currently implemented:"
msgstr ""

#: ../tools.rst:184
msgid "broken symlinks"
msgstr ""

#: ../tools.rst:185
msgid "missing copyright file"
msgstr ""

#: ../tools.rst:186
msgid "obsolete conffiles"
msgstr ""

#: ../tools.rst:187
msgid "Python modules not byte-compiled"
msgstr ""

#: ../tools.rst:188
msgid "``/bin`` and ``/sbin`` binaries requiring ``/usr/lib`` libraries"
msgstr ""

#: ../tools.rst:189
msgid "missing libraries, undefined symbols, symbol size mismatches"
msgstr ""

#: ../tools.rst:190
msgid "license conflicts"
msgstr ""

#: ../tools.rst:191
msgid "program name collisions"
msgstr ""

#: ../tools.rst:192
msgid "missing alternatives"
msgstr ""

#: ../tools.rst:193
msgid "missing ``binfmt`` interpreters and detectors"
msgstr ""

#: ../tools.rst:194
msgid "missing ``pkg-config`` dependencies"
msgstr ""

#: ../tools.rst:197
msgid "``i18nspector``"
msgstr ""

#: ../tools.rst:199
msgid ""
"``i18nspector`` is a tool for checking translation templates (POT), "
"message catalogues (PO) and compiled message catalogues (MO) files for "
"common problems."
msgstr ""

#: ../tools.rst:203
msgid "``cme``"
msgstr ""

#: ../tools.rst:204
msgid ""
"``cme`` is a tool from the ``libconfig-model-dpkg-perl`` package is an "
"editor for dpkg source files with validation. Check the package "
"description to see what it can do."
msgstr ""

#: ../tools.rst:209
msgid "``licensecheck``"
msgstr ""

#: ../tools.rst:210
msgid ""
"``licensecheck`` attempts to determine the license that applies to each "
"file passed to it, by searching the start of the file for text belonging "
"to various licenses."
msgstr ""

#: ../tools.rst:215
msgid "``blhc``"
msgstr ""

#: ../tools.rst:216
msgid "``blhc`` is a tool which checks build logs for missing hardening flags."
msgstr ""

#: ../tools.rst:221
msgid "Helpers for ``debian/rules``"
msgstr "Помощники для ``debian/rules``"

#: ../tools.rst:223
msgid ""
"Package building tools make the process of writing ``debian/rules`` files"
" easier. See :ref:`helper-scripts` for more information about why these "
"might or might not be desired."
msgstr ""
"Инструменты сборки пакетов делают написание файла ``debian/rules`` "
"значительно проще. Дополнительную информацию о том, почему желательно или"
" не желательно использовать их, см. в :ref:`helper-scripts`."

#: ../tools.rst:228
msgid "``debhelper``"
msgstr ""

#: ../tools.rst:230
msgid ""
"``debhelper`` is a collection of programs that can be used in "
"``debian/rules`` to automate common tasks related to building binary "
"Debian packages. ``debhelper`` includes programs to install various files"
" into your package, compress files, fix file permissions, and integrate "
"your package with the Debian menu system."
msgstr ""

#: ../tools.rst:236
msgid ""
"Unlike some approaches, ``debhelper`` is broken into several small, "
"simple commands, which act in a consistent manner. As such, it allows "
"more fine-grained control than some of the other debian/rules tools."
msgstr ""

#: ../tools.rst:240
msgid ""
"There are a number of little ``debhelper`` add-on packages, too transient"
" to document. You can see the list of most of them by doing ``apt-cache "
"search ^dh-``."
msgstr ""
"Имеется ряд небольших дополнительных пакетов ``debhelper``, которые "
"слишком мелки, чтобы описывать их здесь. Вы можете посмотреть список этих"
" программ, выполнив ``apt-cache search ^dh-``."

#: ../tools.rst:244
msgid ""
"When choosing a ``debhelper`` compatibility level for your package, you "
"should choose the highest compatibility level that is supported in the "
"most recent stable release. Only use a higher compatibility level if you "
"need specific features that are provided by that compatibility level that"
" are not available in earlier levels."
msgstr ""

#: ../tools.rst:250
msgid ""
"In the past the compatibility level was defined in ``debian/compat``, "
"however nowadays it is much better to not use that but rather to use a "
"versioned build-dependency like ``debhelper-compat (=12)``."
msgstr ""

#: ../tools.rst:257
msgid "``dh-make``"
msgstr ""

#: ../tools.rst:259
msgid ""
"The ``dh-make`` package contains ``dh_make``, a program that creates a "
"skeleton of files necessary to build a Debian package out of a source "
"tree. As the name suggests, ``dh_make`` is a rewrite of ``debmake``, and "
"its template files use ``dh_*`` programs from ``debhelper``."
msgstr ""

#: ../tools.rst:264
msgid ""
"While the rules files generated by ``dh_make`` are in general a "
"sufficient basis for a working package, they are still just the "
"groundwork: the burden still lies on the maintainer to finely tune the "
"generated files and make the package entirely functional and Policy-"
"compliant."
msgstr ""
"Хотя файлы rules, порождаемые ``dh_make``, вообще-то являются достаточной"
" основой для создания рабочего пакета, они всё равно представляют собой "
"лишь основу: на сопровождающем всё ещё лежит груз задачи по тонкой "
"настройке порождённых файлов и сборке пакета, целиком соответствующего "
"Политике и работающего."

#: ../tools.rst:271
msgid "``equivs``"
msgstr ""

#: ../tools.rst:273
msgid ""
"``equivs`` is another package for making packages. It is often suggested "
"for local use if you need to make a package simply to fulfill "
"dependencies. It is also sometimes used when making *meta-packages*, "
"which are packages whose only purpose is to depend on other packages."
msgstr ""
"``equivs`` представляет собой ещё один пакет для создания пакетов. Часто "
"он предлагается для локального использования, если вам нужно сделать "
"пакет просто для удовлетворения зависимостей. Также он иногда "
"используется при создании „метапакетов“, это пакеты, чья цель состоит "
"лишь в том, чтобы зависеть от других пакетов."

#: ../tools.rst:281
msgid "Package builders"
msgstr "Сборщики пакетов"

#: ../tools.rst:283
msgid ""
"The following packages help with the package building process, general "
"driving of ``dpkg-buildpackage``, as well as handling supporting tasks."
msgstr ""

#: ../tools.rst:287
msgid "``git-buildpackage``"
msgstr ""

#: ../tools.rst:289
msgid ""
"``git-buildpackage`` provides the capability to inject or import Debian "
"source packages into a Git repository, build a Debian package from the "
"Git repository, and helps in integrating upstream changes into the "
"repository."
msgstr ""
"``git-buildpackage`` предоставляет возможность введения или импорта "
"пакетов Debian с исходным кодом в репозиторий Git, сборки пакета Debian "
"из репозитория Git, а также помогает в интеграции изменений из основной "
"ветки разработки в этот репозиторий."

#: ../tools.rst:294
msgid ""
"These utilities provide an infrastructure to facilitate the use of Git by"
" Debian maintainers. This allows one to keep separate Git branches of a "
"package for ``stable``, ``unstable`` and possibly ``experimental`` "
"distributions, along with the other benefits of a version control system."
msgstr ""
"Эти утилиты предоставляют инфраструктуру для облегчения использования Git"
" сопровождающими Debian. Это позволяет хранить отдельные ветки Git пакета"
" для ``стабильного``, ``нестабильного`` и возможно ``экспериментального``"
" выпусков, а также иметь все другие преимущества системы контроля версий."

#: ../tools.rst:303
msgid "``debootstrap``"
msgstr ""

#: ../tools.rst:305
msgid ""
"The ``debootstrap`` package and script allows you to bootstrap a Debian "
"base system into any part of your filesystem. By base system, we mean the"
" bare minimum of packages required to operate and install the rest of the"
" system."
msgstr ""
"Пакет ``debootstrap`` и соответствующий сценарий позволяют вам произвести"
" начальную установку базовой системы Debian в любую часть файловой "
"системы. Под базовой системой мы подразумеваем минимальное число пакетов,"
" необходимых для работы и установки остальной системы."

#: ../tools.rst:310
msgid ""
"Having a system like this can be useful in many ways. For instance, you "
"can ``chroot`` into it if you want to test your build dependencies. Or "
"you can test how your package behaves when installed into a bare base "
"system. Chroot builders use this package; see below."
msgstr ""
"Иметь подобную систему весьма полезно. Например, вы можете сделать "
"``chroot`` в эту систему и проверить ваши сборочные зависимости. Либо вы "
"можете проверить то, как ведут себя ваши пакеты при установке на базовую "
"систему. Сборщики chroot используют этот пакет; см. об этом ниже."

#: ../tools.rst:318
msgid "``pbuilder``"
msgstr ""

#: ../tools.rst:320
msgid ""
"``pbuilder`` constructs a chrooted system, and builds a package inside "
"the chroot. It is very useful to check that a package's build "
"dependencies are correct, and to be sure that unnecessary and wrong build"
" dependencies will not exist in the resulting package."
msgstr ""

#: ../tools.rst:325
msgid ""
"A related package is ``cowbuilder``, which speeds up the build process "
"using a COW filesystem on any standard Linux filesystem."
msgstr ""

#: ../tools.rst:331
msgid "``sbuild``"
msgstr ""

#: ../tools.rst:333
msgid ""
"``sbuild`` is another automated builder. It can use chrooted environments"
" as well. It can be used stand-alone, or as part of a networked, "
"distributed build environment. As the latter, it is part of the system "
"used by porters to build binary packages for all the available "
"architectures. See :ref:`wanna-build` for more information, and "
"https://buildd.debian.org/\\  to see the system in action."
msgstr ""
"``sbuild`` представляет собой другой автоматизированный сборщик. Он также"
" может использовать окружения chroot. Он может использоваться как "
"отдельно, так и как часть сетевого, распределённого окружения. В "
"последнем случае он является частью системы, используемой теми, кто "
"занимается переносом, для сборки двоичных пакетов для всех доступных "
"архитектур. Дополнительную информацию см. в :ref:`wanna-build`, систему в"
" действии можно посмотреть по адресу https://buildd.debian.org/\\ ."

#: ../tools.rst:344
msgid "Package uploaders"
msgstr "ПО для загрузки пакетов"

#: ../tools.rst:346
msgid ""
"The following packages help automate or simplify the process of uploading"
" packages into the official archive."
msgstr ""
"Следующие пакеты помогут автоматизировать или упростить процесс загрузки "
"пакетов в официальный архив."

#: ../tools.rst:352
msgid "``dupload``"
msgstr ""

#: ../tools.rst:354
msgid ""
"``dupload`` is a package and a script to automatically upload Debian "
"packages to the Debian archive, to log the upload, and to send mail about"
" the upload of a package. You can configure it for new upload locations "
"or methods."
msgstr ""
"``dupload`` представляет собой пакет и сценарий для автоматической "
"загрузки пакетов Debian в архив Debian, для записи журнала загрузки, а "
"также отправки сообщений электронной почты о загрузке пакета. Вы можете "
"настроить его на использование новых мест или методов загрузки."

#: ../tools.rst:362
msgid "``dput``"
msgstr ""

#: ../tools.rst:364
msgid ""
"The ``dput`` package and script do much the same thing as ``dupload``, "
"but in a different way. It has some features over ``dupload``, such as "
"the ability to check the GnuPG signature and checksums before uploading, "
"and the possibility of running ``dinstall`` in dry-run mode after the "
"upload."
msgstr ""

#: ../tools.rst:373
msgid "``dcut``"
msgstr ""

#: ../tools.rst:375
msgid ""
"The ``dcut`` script (part of the package ``dput``, :ref:`dput`) helps in "
"removing files from the ftp upload directory."
msgstr ""
"Сценарий ``dcut`` (часть пакета ``dput``, :ref:`dput`) помогает удалять "
"файлы из каталога загрузки на ftp."

#: ../tools.rst:381
msgid "Maintenance automation"
msgstr "Автоматизация сопровождения пакетов"

#: ../tools.rst:383
msgid ""
"The following tools help automate different maintenance tasks, from "
"adding changelog entries or signature lines and looking up bugs in Emacs "
"to making use of the newest and official ``config.sub``."
msgstr ""
"Следующие инструменты помогают автоматизировать различные задачи по "
"сопровождению пакетов от добавления записей в журнал изменений или строк "
"подписи и поиска ошибок в Emacs до использования наиболее свежего и "
"исключительно официального файла ``config.sub``."

#: ../tools.rst:390
msgid "``devscripts``"
msgstr ""

#: ../tools.rst:392
msgid ""
"``devscripts`` is a package containing wrappers and tools that are very "
"helpful for maintaining your Debian packages. Example scripts include "
"``debchange`` (or its alias, ``dch``), which manipulates your "
"``debian/changelog`` file from the command-line, and ``debuild``, which "
"is a wrapper around ``dpkg-buildpackage``. The ``bts`` utility is also "
"very helpful to update the state of bug reports on the command line. "
"``uscan`` can be used to watch for new upstream versions of your "
"packages. ``suspicious-source`` outputs a list of files which are not "
"common source files."
msgstr ""

#: ../tools.rst:402
msgid "See the devscripts 1 manual page for a complete list of available scripts."
msgstr "Полный список доступных сценариев см. в руководстве devscripts 1."

#: ../tools.rst:406
msgid "``reportbug``"
msgstr ""

#: ../tools.rst:408
msgid ""
"``reportbug`` is a tool designed to make the reporting of bugs in Debian "
"and derived distributions relatively painless.  Its features include:"
msgstr ""

#: ../tools.rst:411
msgid "Integration with mutt and mh/nmh mail readers."
msgstr ""

#: ../tools.rst:412
msgid ""
"Access to outstanding bug reports to make it easier to identify whether "
"problems have already been reported."
msgstr ""

#: ../tools.rst:414
msgid "Automatic checking for newer versions of packages."
msgstr ""

#: ../tools.rst:416
msgid ""
"``reportbug`` is designed to be used on systems with an installed mail "
"transport agent; however, you can edit the configuration file and send "
"reports using any available mail server."
msgstr ""

#: ../tools.rst:420
msgid ""
"This package also includes the ``querybts`` script for browsing the "
"Debian `bug tracking system <https://www.debian.org/Bugs/>`__."
msgstr ""

#: ../tools.rst:424
msgid "``autotools-dev``"
msgstr ""

#: ../tools.rst:426
msgid ""
"``autotools-dev`` contains best practices for people who maintain "
"packages that use ``autoconf`` and/or ``automake``. Also contains "
"canonical ``config.sub`` and ``config.guess`` files, which are known to "
"work on all Debian ports."
msgstr ""

#: ../tools.rst:432
msgid "``dpkg-repack``"
msgstr ""

#: ../tools.rst:434
msgid ""
"``dpkg-repack`` creates a Debian package file out of a package that has "
"already been installed. If any changes have been made to the package "
"while it was unpacked (e.g., files in ``/etc`` were modified), the new "
"package will inherit the changes."
msgstr ""

#: ../tools.rst:439
msgid ""
"This utility can make it easy to copy packages from one computer to "
"another, or to recreate packages that are installed on your system but no"
" longer available elsewhere, or to save the current state of a package "
"before you upgrade it."
msgstr ""

#: ../tools.rst:445
msgid "``alien``"
msgstr ""

#: ../tools.rst:447
msgid ""
"``alien`` converts binary packages between various packaging formats, "
"including Debian, RPM (RedHat), LSB (Linux Standard Base), Solaris, and "
"Slackware packages."
msgstr ""
"``alien`` преобразует пакеты между различными форматами пакетов, включая "
"пакеты Debian, RPM (RedHat), LSB (Linux Standard Base), Solaris, и "
"Slackware."

#: ../tools.rst:454
msgid "``dpkg-dev-el``"
msgstr ""

#: ../tools.rst:456
msgid ""
"``dpkg-dev-el`` is an Emacs lisp package that provides assistance when "
"editing some of the files in the ``debian`` directory of your package. "
"For instance, there are handy functions for listing a package's current "
"bugs, and for finalizing the latest entry in a ``debian/changelog`` file."
msgstr ""

#: ../tools.rst:465
msgid "``dpkg-depcheck``"
msgstr ""

#: ../tools.rst:467
msgid ""
"``dpkg-depcheck`` (from the ``devscripts`` package, :ref:`devscripts`) "
"runs a command under ``strace`` to determine all the packages that were "
"used by the said command."
msgstr ""
"``dpkg-depcheck`` (из пакета ``devscripts``, :ref:`devscripts`) запускает"
" команду в окружении ``strace`` для определения всех пакетов, которые "
"используются вызванной командой."

#: ../tools.rst:471
msgid ""
"For Debian packages, this is useful when you have to compose a ``Build-"
"Depends`` line for your new package: running the build process through "
"``dpkg-depcheck`` will provide you with a good first approximation of the"
" build-dependencies. For example:"
msgstr ""
"Для пакетов Debian это весьма полезно, если вам необходимо сформировать "
"строку ``Build-Depends`` для вашего нового пакета: запуск процесса сборки"
" через ``dpkg-depcheck`` предоставит вам приблизительный список сборочных"
" зависимостей. Например:"

#: ../tools.rst:480
msgid ""
"``dpkg-depcheck`` can also be used to check for run-time dependencies, "
"especially if your package uses exec 2 to run other programs."
msgstr ""
"``dpkg-depcheck`` также может использоваться для проверки зависимостей "
"времени исполнения, особенно в том случае, если ваш пакет использует exec"
" 2 для запуска других программ."

#: ../tools.rst:483
msgid "For more information please see dpkg-depcheck 1."
msgstr "Дополнительную информацию см. в dpkg-depcheck 1."

#: ../tools.rst:488
msgid "Porting tools"
msgstr "Инструменты для переноса"

#: ../tools.rst:490
msgid "The following tools are helpful for porters and for cross-compilation."
msgstr ""
"Следующие инструменты полезны для занимающихся переносом и "
"кросс-компиляцией."

#: ../tools.rst:493
msgid "``dpkg-cross``"
msgstr ""

#: ../tools.rst:495
msgid ""
"``dpkg-cross`` is a tool for installing libraries and headers for cross-"
"compiling in a way similar to ``dpkg``. Furthermore, the functionality of"
" ``dpkg-buildpackage`` and ``dpkg-shlibdeps`` is enhanced to support "
"cross-compiling."
msgstr ""
"``dpkg-cross`` является инструментом для установки библиотек и "
"заголовочных файлов для перекрёстной компиляции схожим с ``dpkg`` "
"способом. Более того, функциональность ``dpkg-buildpackage`` и ``dpkg-"
"shlibdeps`` была улучшена в плане поддержки перекрёстной компиляции."

#: ../tools.rst:503
msgid "Documentation and information"
msgstr "Документация и информацию"

#: ../tools.rst:505
msgid ""
"The following packages provide information for maintainers or help with "
"building documentation."
msgstr ""
"Следующие пакеты предоставляют информацию для сопровождающих, либо "
"помогают им в сборке документации."

#: ../tools.rst:509
msgid "``debian-policy``"
msgstr ""

#: ../tools.rst:511
msgid ""
"The ``debian-policy`` package contains the Debian Policy Manual and "
"related documents, which are:"
msgstr ""

#: ../tools.rst:514
msgid "Debian Policy Manual"
msgstr ""

#: ../tools.rst:515
msgid "Filesystem Hierarchy Standard (FHS)"
msgstr ""

#: ../tools.rst:516
msgid "Debian Menu sub-policy"
msgstr ""

#: ../tools.rst:517
msgid "Debian Perl sub-policy"
msgstr ""

#: ../tools.rst:518
msgid "Debian configuration management specification"
msgstr ""

#: ../tools.rst:519
msgid "Machine-readable debian/copyright specification"
msgstr ""

#: ../tools.rst:520
msgid "Autopkgtest - automatic as-installed package testing"
msgstr ""

#: ../tools.rst:521
msgid "Authoritative list of virtual package names"
msgstr ""

#: ../tools.rst:522
msgid "Policy checklist for upgrading your packages"
msgstr ""

#: ../tools.rst:524
msgid ""
"The Debian Policy Manual the policy relating to packages and details of "
"the packaging mechanism. It covers everything from required ``gcc`` "
"options to the way the maintainer scripts (``postinst`` etc.) work, "
"package sections and priorities, etc."
msgstr ""

#: ../tools.rst:529
msgid ""
"Also useful is the file ``/usr/share/doc/debian-policy/upgrading-"
"checklist.txt.gz``, which lists changes between versions of policy."
msgstr ""

#: ../tools.rst:534
msgid "``doc-debian``"
msgstr ""

#: ../tools.rst:536
msgid "``doc-debian`` contains lots of useful Debian-specific documentation:"
msgstr ""

#: ../tools.rst:538
msgid "Debian Linux Manifesto"
msgstr ""

#: ../tools.rst:539
msgid "Constitution for the Debian Project"
msgstr ""

#: ../tools.rst:540
msgid "Debian Social Contract"
msgstr ""

#: ../tools.rst:541
msgid "Debian Free Software Guidelines"
msgstr ""

#: ../tools.rst:542
msgid "Debian Bug Tracking System documentation"
msgstr ""

#: ../tools.rst:543
msgid "Introduction to the Debian mailing lists"
msgstr ""

#: ../tools.rst:546
msgid "``developers-reference``"
msgstr ""

#: ../tools.rst:548
msgid ""
"The ``developers-reference`` package contains the document you are "
"reading right now, the Debian Developer's Reference, a set of guidelines "
"and best practices which has been established by and for the community of"
" Debian developers."
msgstr ""

#: ../tools.rst:554
msgid "``maint-guide``"
msgstr ""

#: ../tools.rst:556
msgid "The ``maint-guide`` package contains the Debian New Maintainers' Guide."
msgstr ""

#: ../tools.rst:558
msgid ""
"This document tries to describe the building of a Debian package to "
"ordinary Debian users and prospective developers. It uses fairly non-"
"technical language, and it's well covered with working examples."
msgstr ""

#: ../tools.rst:563
msgid "``packaging-tutorial``"
msgstr ""

#: ../tools.rst:565
msgid ""
"This tutorial is an introduction to Debian packaging. It teaches "
"prospective developers how to modify existing packages, how to create "
"their own packages,  and how to interact with the Debian community."
msgstr ""

#: ../tools.rst:569
msgid ""
"In addition to the main tutorial, it includes three practical sessions on"
" modifying the ``grep`` package, and packaging the ``gnujump`` game and a"
" Java library."
msgstr ""

#: ../tools.rst:574
msgid "``how-can-i-help``"
msgstr ""

#: ../tools.rst:576
msgid ""
"``how-can-i-help`` shows opportunities for contributing to Debian. ``how-"
"can-i-help`` hooks into ``APT`` to list opportunities for contributions "
"to Debian (orphaned packages, bugs tagged 'newcomer') for packages "
"installed locally, after each ``APT`` invocation. It can also be invoked "
"directly, and then lists all opportunities for contribution (not just the"
" new ones)."
msgstr ""

#: ../tools.rst:583
msgid "``docbook-xml``"
msgstr ""

#: ../tools.rst:585
msgid ""
"``docbook-xml`` provides the DocBook XML DTDs, which are commonly used "
"for Debian documentation (as is the older debiandoc SGML DTD). This "
"manual, for instance, is written in DocBook XML."
msgstr ""
"``docbook-xml`` предоставляет DocBook XML DTD, который обычно "
"используется для подготовки документации по Debian (как более старая "
"документация debiandoc SGML DTD). Например, данное руководство написано в"
" DocBook XML."

#: ../tools.rst:589
msgid ""
"The ``docbook-xsl`` package provides the XSL files for building and "
"styling the source to various output formats. You will need an XSLT "
"processor, such as ``xsltproc``, to use the XSL stylesheets. "
"Documentation for the stylesheets can be found in the various ``docbook-"
"xsl-doc-*`` packages."
msgstr ""
"Пакет ``docbook-xsl`` предоставляет файлы XSL для сборки и форматирования"
" стиля исходных файлов в различные форматы вывода. Для того, чтобы "
"использовать стили XSL, вам потребуется процессор XSLT, такой как "
"``xsltproc``. Документация для стилей может быть найдена в пакетах "
"``docbook-xsl-doc-*``."

#: ../tools.rst:595
msgid ""
"To produce PDF from FO, you need an FO processor, such as ``xmlroff`` or "
"``fop``. Another tool to generate PDF from DocBook XML is ``dblatex``."
msgstr ""
"Чтобы создать PDF из FO, вам потребуется процессор FO, такой как "
"``xmlroff`` или ``fop``. Другим инструментом для создания PDF из DocBook "
"XML является ``dblatex``."

#: ../tools.rst:599
msgid "``debiandoc-sgml``"
msgstr ""

#: ../tools.rst:601
#, fuzzy
msgid ""
"``debiandoc-sgml`` provides the DebianDoc SGML DTD, which has been "
"commonly used for Debian documentation, but is now deprecated (``docbook-"
"xml`` or ``python3-sphinx`` should be used instead)."
msgstr ""
"``debiandoc-sgml`` предоставляет DebianDoc SGML DTD, который широко "
"используется для подготовки документации по Debian, но в настоящее время "
"устарел (следует использовать пакет ``docbook-xml``). Кроме того, он "
"предоставляет сценарии для сборки и изменения стиля оформления исходного "
"файла в других форматах вывода."

#: ../tools.rst:606
msgid "``debian-keyring``"
msgstr ""

#: ../tools.rst:608
msgid ""
"Contains the public GPG keys of Debian Developers and Maintainers. See "
":ref:`key-maint` and the package documentation for more information."
msgstr ""
"Содержит открытые ключи GPG разработчиков и сопровождающих Debian. "
"Дополнительную информацию см. в :ref:`key-maint` и документации по "
"пакету."

#: ../tools.rst:612
msgid "``debian-el``"
msgstr ""

#: ../tools.rst:614
msgid ""
"``debian-el`` provides an Emacs mode for viewing Debian binary packages. "
"This lets you examine a package without unpacking it."
msgstr ""
"``debian-el`` предоставляет режим Emacs для просмотра двоичных пакетов "
"Debian. Это позволяет вам исследовать пакет без его распаковки."

#~ msgid ""
#~ "``devscripts`` is a package containing "
#~ "wrappers and tools that are very "
#~ "helpful for maintaining your Debian "
#~ "packages. Example scripts include "
#~ "``debchange`` (or its alias, ``dch``), "
#~ "which manipulates your ``debian/changelog`` "
#~ "file from the command-line, and "
#~ "``debuild``, which is a wrapper around"
#~ " ``dpkg-buildpackage``. The ``bts`` utility"
#~ " is also very helpful to update "
#~ "the state of bug reports on the"
#~ " command line. ``uscan`` can be used"
#~ " to watch for new upstream versions"
#~ " of your packages."
#~ msgstr ""

#~ msgid ""
#~ "Documentation for the DTD can be "
#~ "found in the ``debiandoc-sgml-doc`` "
#~ "package."
#~ msgstr ""
#~ "Документация для DTD может быть найдена"
#~ " в пакете ``debiandoc-sgml-doc``."

